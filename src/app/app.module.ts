import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactListComponent } from './components/contact/contact-list/contact-list.component';
import { ContactDetailsComponent } from './components/contact/contact-details/contact-details.component';
import { ContactCreationComponent } from './components/contact/contact-creation/contact-creation.component';
import { ContactUpdateComponent } from './components/contact/contact-update/contact-update.component';
import { HomeComponent } from './components/home/home.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { ContactService } from './services/contact.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ControlsErrorComponent } from './components/validatorMessage/controls-error/controls-error.component';
import { PromptDialogComponent } from './components/prompt-dialog/prompt-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import {MatButtonModule } from '@angular/material/button';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactDetailsComponent,
    ContactCreationComponent,
    ContactUpdateComponent,
    HomeComponent,
    SidebarComponent,
    HeaderComponent,
    ControlsErrorComponent,
    PromptDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatCommonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [ContactService],
  bootstrap: [AppComponent],
  entryComponents:[PromptDialogComponent]
})
export class AppModule { }
