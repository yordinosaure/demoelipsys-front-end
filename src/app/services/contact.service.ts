import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { Contact } from '../models/Contact';
import { apiRoutes } from '../_shared/Constant';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  url: string = apiRoutes.baseUrl + apiRoutes.urls.contact;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Contact[]> {
     return this.http.get<Contact[]>(this.url);
  }

  getById(id: string): Observable<Contact> {
    return this.http.get<Contact>(this.url  + id);
  }

  create(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.url, contact);
  }

  delete(id: string): Observable<string> {
    return this.http.delete<string>(this.url  + id);
  }

  update(contact: Contact): Observable<Contact> {
    return this.http.put<Contact>(this.url + contact.id, contact);
  }
}
