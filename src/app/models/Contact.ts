export class Contact {

    id: string;
    firstName: string;
    lastName: string;
    address: string;
    number: string;
    zipCode: string;
    city: string;
    avatarUrl: string;
    phoneNumber: string;
    email: string;
    
}