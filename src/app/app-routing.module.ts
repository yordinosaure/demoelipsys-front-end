import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactListComponent } from './components/contact/contact-list/contact-list.component';
import { ContactCreationComponent } from './components/contact/contact-creation/contact-creation.component';
import { ContactDetailsComponent } from './components/contact/contact-details/contact-details.component';
import { ContactUpdateComponent } from './components/contact/contact-update/contact-update.component';


const routes: Routes = [
  {path: '', component: ContactListComponent},
  {path: 'List', component: ContactListComponent},
  {path: 'Create', component: ContactCreationComponent},
  {path: 'Update/:id', component: ContactUpdateComponent},
  {path: 'Details/:id', component: ContactDetailsComponent},
  {path: '**', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
