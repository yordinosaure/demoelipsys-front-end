import { Component, OnInit, ElementRef, AfterViewInit, ViewChild} from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';
import { Observable, fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Contact } from 'src/app/models/Contact';
import { Router } from '@angular/router';
import {MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PromptDialogComponent } from '../../prompt-dialog/prompt-dialog.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  @ViewChild('input') input: ElementRef;
  // $contacts: Observable<Contact[]>;
  contacts: Contact [] = [];
  searchFields: string[] = [];
  contactSearched: Contact[]= [];
  selectedContact: Contact = new Contact();

  startIndex: number = 0;
  endIndex: number;
  range: number = 10;
  ready: boolean = false;

  delConfirmed: boolean = false;

  constructor(private contactService: ContactService, private router: Router, private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.endIndex = this.startIndex + this.range;
    // this.$contacts = this.contactService.getAll().pipe();
    this.contactService.getAll().subscribe(x =>{
      this.contacts = x;
      this.ready = true;
      this.searchFields = Object.getOwnPropertyNames(this.contacts[0]);
    });
        
  }

  ngAfterViewInit() {
  fromEvent(this.input.nativeElement,'keyup')
      .pipe(
          debounceTime(150),
          distinctUntilChanged(),
          tap((text) => {          
              this.searchCollection(this.input.nativeElement.value);          
          })
      )
      .subscribe();
  }

  toCreate(){
    this.router.navigateByUrl('/Create');
  }

  searchCollection(str: string){
    this.contactSearched = [];
    if(this.input.nativeElement.value != ''){
      this.contacts.forEach(x=>{
        this.searchFields.forEach(y=>{
          if(x[y].toLowerCase().includes(str.toLowerCase())){
            if(this.contactSearched.indexOf(x) == -1){
              this.contactSearched.push(x);
            }
          }
        })
      })
      this.contacts.sort((a)=>{
        if(this.contactSearched.includes(a)){
          return 0;
        }
        else {
          return 1;
        }
      });
    }
    if(this.contactSearched.length == 1){
      this.selectedContact = this.contactSearched[0];
    }
  }

  next() {
    if(this.endIndex + this.range < this.contacts.length){
      this.startIndex += this.range;
      this.endIndex += this.range;
    }
    else if(this.startIndex + this.range < this.contacts.length){
      this.startIndex += this.range;
      this.endIndex = this.contacts.length;
    }
  }

  previous() {
    if(this.startIndex - this.range >= 0){
      this.startIndex -= this.range;
      this.endIndex = this.startIndex + this.range;
    }
  }

  toUpdate(id){
    this.router.navigateByUrl('/Update/'+ id);
  }

  selectContact(contact: Contact){
    this.selectedContact = contact;
  }

  unselect(){
    this.selectedContact = new Contact();
  }

  suppress(id: string){
    let dialogRef = this.matDialog.open(PromptDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.contactService.delete(id).toPromise().then(()=>{
          let i = this.contacts.findIndex(x=>x.id == id);
          if(i != -1) {
            this.contacts.splice(i, 1);
          }
        });
      }     
    });
  }

  
}
