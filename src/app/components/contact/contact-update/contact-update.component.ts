import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/models/Contact';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-contact-update',
  templateUrl: './contact-update.component.html',
  styleUrls: ['./contact-update.component.scss']
})
export class ContactUpdateComponent implements OnInit {

  form: FormGroup;
  contact: Contact;
  ready: boolean = false;

  constructor(private fb: FormBuilder, private contactService: ContactService,
             private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;
    this.contactService.getById(id).subscribe(x=>{
      this.contact = x;
      console.log(this.contact)
      this.setFormValue();
      this.ready = true;
    })
    this.initForm();
  }

  initForm(){
    this.form = this.fb.group({
      firstName: [, [Validators.required]],
      lastName: [,[Validators.required]],
      email: [,[Validators.required, Validators.email]],
      address: [, [Validators.required]],
      number: [, [Validators.required]],
      zipCode: [, [Validators.required, Validators.pattern('^[0-9]{4}$')]],
      city: [, [Validators.required]],
      phoneNumber: [,[Validators.required]],
      // avatarUrl: ['']
    },
    { updateOn: "blur" });
  }

  setFormValue(){
    this.form.controls.firstName.setValue(this.contact.firstName);
    this.form.controls.lastName.setValue(this.contact.lastName);
    this.form.controls.email.setValue(this.contact.email);
    this.form.controls.address.setValue(this.contact.address);
    this.form.controls.number.setValue(this.contact.number);
    this.form.controls.zipCode.setValue(this.contact.zipCode);
    this.form.controls.city.setValue(this.contact.city);
    this.form.controls.phoneNumber.setValue(this.contact.phoneNumber);
  }

  save() {
    this.contact.firstName = this.form.value.firstName;
    this.contact.lastName = this.form.value.lastName;
    this.contact.email = this.form.value.email;
    this.contact.address = this.form.value.address;
    this.contact.number = this.form.value.number;
    this.contact.zipCode = this.form.value.zipCode.toString();
    this.contact.city = this.form.value.city;
    this.contact.phoneNumber = this.form.value.phoneNumber.toString();
    // this.contact.avatarUrl = this.form.value.avatarUrl? this.form.value.avatarUr:'';
    console.log(this.contact);
    this.contactService.update(this.contact).toPromise().then(()=>{
      this.router.navigateByUrl('/');
    });
  }

  cancel() {
    this.router.navigateByUrl('/');
  }
}
