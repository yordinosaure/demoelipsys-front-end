import { Component, OnInit } from '@angular/core';

import { FormBuilder, Validators, FormGroup, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { Contact } from 'src/app/models/Contact';
import { ContactService } from 'src/app/services/contact.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-creation',
  templateUrl: './contact-creation.component.html',
  styleUrls: ['./contact-creation.component.scss']
})
export class ContactCreationComponent implements OnInit {

  

  form: FormGroup;

  constructor(private fb: FormBuilder, private contactService: ContactService, private router: Router) {
    
   }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.form = this.fb.group({
      firstName: [, [Validators.required]],
      lastName: [,[Validators.required]],
      email: [,[Validators.required, Validators.email]],
      address: [, [Validators.required]],
      number: [, [Validators.required]],
      zipCode: [, [Validators.required, Validators.pattern('^[0-9]{4}$')]],
      city: [, [Validators.required]],
      phoneNumber: [,[Validators.required]],
      // avatarUrl: ['']
    },
    { updateOn: "blur" }
    );
  }

  save() {
    const contact: Contact = new Contact();
    contact.firstName = this.form.value.firstName;
    contact.lastName = this.form.value.lastName;
    contact.email = this.form.value.email;
    contact.address = this.form.value.address;
    contact.number = this.form.value.number;
    contact.zipCode = this.form.value.zipCode.toString();
    contact.city = this.form.value.city;
    contact.phoneNumber = this.form.value.phoneNumber.toString();
    contact.avatarUrl = this.form.value.avatarUrl? this.form.value.avatarUr:'https://robohash.org/autaliasaccusamus.bmp?size=50x50&set=set1';
    console.log(contact);
    this.contactService.create(contact).toPromise().then(()=>{
      this.router.navigateByUrl('/');
    });
  }

  reset(){
    this.initForm();
  }
}
