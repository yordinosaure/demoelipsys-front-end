import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlsErrorComponent } from './controls-error.component';

describe('ControlsErrorComponent', () => {
  let component: ControlsErrorComponent;
  let fixture: ComponentFixture<ControlsErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlsErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
