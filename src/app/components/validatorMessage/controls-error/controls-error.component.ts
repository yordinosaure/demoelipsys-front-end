import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-controls-error',
  templateUrl: './controls-error.component.html',
  styleUrls: ['./controls-error.component.scss']
})
export class ControlsErrorComponent implements  OnChanges {

  // @Input() control: any;
  @Input() errors: any;

  messages: string[] = [];
  value:any;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.errors);
    this.messages = [];
    if(!changes.errors.firstChange){
      console.log(changes.errors)
      this.getErrors(changes.errors)
    }
  }

  getErrors(err){
    if(err.currentValue != null){
      let errProps = Object.getOwnPropertyNames(err.currentValue);
      errProps.forEach(x=>{
        if(x == 'email'){
          this.messages.push('Format de mail non-correct')
        }
        if(x == 'required'){
          this.messages.push('Champs recquis')
        }
        if(x == 'pattern'){
          if(err.currentValue[x]['requiredPattern']== "^[0-9]{4}$"){
            this.messages.push('Format atendu: 9999')
          }
        }
      });
    }
  }

}
